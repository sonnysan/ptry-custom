import axios from "axios";

export async function getData() {
    try{
        let className='main__row';
        const uri = window.location.pathname + window.location.search;
        let url = `https://api.allorigins.win/get?url=https://www.porntry.com${uri}`;
        if(uri.startsWith("/videos/")) {
            className = 'block-related';
            // url = `https://www.porntry.com${uri}`
        }
        const response = await axios.get(url)
        const parser = new DOMParser();
        const htmlDoc = parser.parseFromString(response.data.contents, 'text/html');
        const rows = htmlDoc.getElementsByClassName(className);
        const vid = htmlDoc.getElementsByClassName('video-block');
        if(vid.length > 0) {
            let vb = vid[0];
            vb.removeChild(htmlDoc.getElementById('player'));
            const ifrm = document.getElementById('ifvid');
            ifrm.setAttribute('sandbox', "allow-scripts");
            ifrm.setAttribute("src", `https://www.porntry.com${uri}`);
            ifrm.style.width = "100%";
            ifrm.style.height = "480px";
            ifrm.onload = function(e) {
                console.log('iframe: ', e.target);
            }
            return[rows, vid]
        }
        return [rows, null]
    } catch (e) {
        alert(e.message);
    }
}