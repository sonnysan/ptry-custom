import './App.css';
import { getData } from './api/ptry';
import { useEffect, useState } from 'react';

function App() {
  const [done, setDone] =useState(false);
  useEffect(() => {
    console.log("sss");
    getData().then((res) => {
      let [r, v] = res;
      if(done) {
        return;
      }
      if(v) {
        document.getElementById("app").appendChild(v);
      }
      while (r.length > 0){
        document.getElementById("app").appendChild(r[0]);
      }
      const thumbs = document.getElementsByClassName('thumb__img');
      console.log(thumbs);
      for(let i of thumbs) {
        const src = i.getAttribute('data-preview');
        const video = document.createElement("video");
        video.preload = 'metadata';
        video.src = src;
        // Load video in Safari / IE11
        video.muted = true;
        video.playsInline = true;
        video.controls = true;
        video.poster = i.getElementsByTagName('img')[0].src;
        i.append(video);
        i.firstElementChild.remove();
        const tim = i.firstElementChild;
        i.firstElementChild.remove();
        i.append(tim);
      }
      let links = document.getElementsByTagName('a');
      for(let a of links) {
        let href = a.getAttribute('href');
        if(!a.classList.contains('video-links__link')) {
          if(href) {
            a.setAttribute("href", href.replace('https://www.porntry.com/', '/'));
          }
        }
      }
      setDone(true);
    });
    // eslint-disable-next-line
  }, []);
  const submit = (e) => {
    e.preventDefault();
    const data = new FormData(e.target);
    const search = data.get('search').replace(" ", "-");
    window.location = `/search/${search}/`
  }
  return (
    <div className="App">
      <header className="App-header">
        {/* <img src={logo} className="App-logo" alt="logo" /> */}
        <form onSubmit={submit}>
          <input name="search" type="text" placeholder='Search' required/>
          <hr/>
          <input type={"submit"} value="search" />
        </form>
      </header>
      <br/><br/>
      <div className='if-wrapper'>
        <iframe id="ifvid" title="video"></iframe>
      </div>
      <div id="app">
      </div>
    </div>
  );
}

export default App;
